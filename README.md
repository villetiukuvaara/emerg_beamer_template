# EMERG_Beamer_Template

LaTeX (Beamer) clone of GM PowerPoint template.

The latest version is kept at https://villetiukuvaara@bitbucket.org/villetiukuvaara/emerg_beamer_template.git

This should compile fine with the normal pdfLaTeX compiler. The previous version required XeLaTeX because I was using the fontspec package.